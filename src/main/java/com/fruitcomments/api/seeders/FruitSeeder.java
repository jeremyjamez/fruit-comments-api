package com.fruitcomments.api.seeders;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.repositories.IFruitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FruitSeeder {

    @Autowired
    private IFruitRepository fruitRepo;

    @EventListener
    public void seed(ContextRefreshedEvent ev){
        Fruit[] fruits = new Fruit[]{
                new Fruit(1, "Orange", "It is orange", "orange.jpg"),
                new Fruit(2, "Apple", "Taste good", "apple.jpg"),
                new Fruit(3, "Strawberry", "Seeds on outside", "strawberry.jpg")
        };
        System.out.println("---------- Seeding -----------");
        for(Fruit f : fruits){
            Example<Fruit> fruitExample = Example.of(f);
            Optional<Fruit> fruitResult = fruitRepo.findOne(fruitExample);

            if(!fruitResult.isPresent()){
                fruitRepo.save(f);
                System.out.println("Added fruit: " + f);
            } else {
                System.out.println("Fruit already added " + f);
            }
        }
        System.out.println("---------- Seeding Complete -----------");
    }
}
