package com.fruitcomments.api.repositories;

import com.fruitcomments.api.entities.Fruit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFruitRepository extends JpaRepository<Fruit, Integer> {


}
