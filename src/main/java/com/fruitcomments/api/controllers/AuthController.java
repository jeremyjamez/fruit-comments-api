package com.fruitcomments.api.controllers;

import com.fruitcomments.api.viewmodels.LoginViewModel;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    @GetMapping("/hello")
    public String hello(){
        return "Hello from the other side";
    }

    @GetMapping("/sayHello")
    public String sayHello(
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") Optional<String> lastName){
        return String.format("Hello %s %s", firstName, lastName.orElse(""));
    }

    @GetMapping("/user/{username}")
    public LoginViewModel getUser(@PathVariable("username") String username) {
        return new LoginViewModel(username, "never send password back to user");
    }

    @PostMapping("/login")
    public String login(
            @RequestBody LoginViewModel credentials
    ){
        return String.format("Fake-Token-%s-%s", credentials.getUsername(), credentials.getPassword());
    }
}
