package com.fruitcomments.api.controllers;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.services.FruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/fruits")
public class FruitController {

    @Autowired
    protected FruitService service;

    @GetMapping("")
    public List<Fruit> getFruits() {
        return service.getAll();
    }

}
